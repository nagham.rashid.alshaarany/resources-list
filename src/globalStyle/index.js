import { createGlobalStyle } from 'styled-components';
import InterSemiBold from 'assets/fonts/Inter-SemiBold.ttf';
import InterRegular from 'assets/fonts/Inter-Regular.ttf';
import { device, colors, shadow } from '../Theme';
const GlobalStyles = createGlobalStyle`

body { 
  background-color: ${colors.white};
  margin: 0;
  padding: 0;
  box-sizing: border-box;
  font-family: 'Inter', sans-serif;

  min-width: ${device.mobileS};
}

ul { 
  margin: 0;
  padding: 0;
}

a {
  text-decoration: none;
  color: inherit;
}

// FONTS TYPE

// 600 / Semi Bold
@font-face {
  font-family: 'InterSemiBold';
  font-style: normal;
  font-weight: 600;
  font-display: fallback;
  src: local('Inter-SemiBold'),
    url(${InterSemiBold}) format('truetype');
}


// 400 / Reqular / Normal
@font-face {
  font-family: 'InterRegular';
  font-style: normal;
  font-weight: normal;
  font-display: fallback;
  src: local('Inter-Regular'),
    url(${InterRegular}) format('truetype');
}`;

export { GlobalStyles };
