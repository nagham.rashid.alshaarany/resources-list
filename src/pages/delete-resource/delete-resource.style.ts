import styled from 'styled-components';
import { Button, Typography } from '../../components';
import { colors } from '../../Theme';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Title = styled(Typography).attrs({ variant: 'semibold', size: 'xs', color: colors.gray[900] })`
  margin-top: 16px;
`;

export const Text = styled(Typography).attrs({ variant: 'normal', size: 'md', color: colors.gray[600] })`
  margin-top: 8px;
  margin-bottom: 24px;
`;

export const DeleteButton = styled(Button).attrs({ variant: 'contained', size: 'xs', color: 'danger' })``;