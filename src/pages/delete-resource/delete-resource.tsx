import React, { useCallback, useState } from 'react';
import { Modal } from '../../components';
import { Container, DeleteButton, Title, Text } from './delete-resource.style';

interface DeleteResourceProps {
  isOpen: boolean;
  onClose: () => void;
  onSubmit: () => void;
  data: {
    title: string;
  };
}

const DeleteResource = ({ isOpen, onClose, onSubmit, data }: DeleteResourceProps) => {
  return (
    <Modal isOpen={isOpen} onClose={onClose} varaint="danger">
      <Container>
        <Title>Delete the existing resource</Title>
        <Text> {data.title} </Text>
        <DeleteButton onClick={() => onSubmit()}>Delete</DeleteButton>
      </Container>
    </Modal>
  );
};

export default DeleteResource;