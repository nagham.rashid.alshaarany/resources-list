import styled from "styled-components";
import Typography from "../../components/Typography";
import { size } from "../../Theme/breakpoints";
import { primary } from "../../Theme/colors";

export const Container = styled.div`
  padding-top: 100px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  padding-right: 16px;
  padding-left: 16px;

  margin-right: auto;
  margin-left: auto;

  max-width: ${size.laptopL};
`;
export const Title = styled(Typography).attrs({ variant: 'semibold', size: 'lg', color: primary[900] })`
  margin-top: 16px;
`;