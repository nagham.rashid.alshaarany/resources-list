import React from 'react';
import { Container, Title } from './errorMessage.style';

const ErrorPage = () => {
  return (
    <Container>
      <Title>Oops!! Something went wrong</Title>
    </Container>
  );
};

export default ErrorPage;
