import React, { useCallback, useState } from 'react';
import { Modal } from '../../components';
import { Container, SubmitButton, Title, Input } from './edit-resource.style';

interface EditResourceProps {
  isOpen: boolean;
  onClose: () => void;
  onSubmit: (arg0: any) => void;
  data: {
    title: string;
    body: string;
  };
}

const EditResource = ({ isOpen, onClose, onSubmit, data }: EditResourceProps) => {
  const [title, setTitle] = useState(data.title);
  const [body, setBody] = useState(data.body);

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <Container>
        <Title>Edit the existing resource</Title>
        <Input placeholder="Enter the title" value={title} onChange={(e) => setTitle(e.target.value)} />
        <Input placeholder="Enter the body" value={body} onChange={(e) => setBody(e.target.value)} />
        <SubmitButton onClick={() => onSubmit({ title, body })}>Update</SubmitButton>
      </Container>
    </Modal>
  );
};

export default EditResource;
