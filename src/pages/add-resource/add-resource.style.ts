import styled from 'styled-components';
import { Button, Typography } from '../../components';
import { colors } from '../../Theme';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
`;

export const Title = styled(Typography).attrs({ variant: 'semibold', size: 'xs', color: colors.gray[900] })`
  margin-top: 16px;
  margin-bottom: 24px;
`;

export const Input = styled.input`
  padding: 10px 14px;
  background: #ffffff;
  border: 1px solid #d0d5dd;
  box-shadow: 0px 1px 2px rgba(16, 24, 40, 0.05);
  border-radius: 8px;

  margin-bottom: 24px;
`;

export const SubmitButton = styled(Button).attrs({ variant: 'contained', size: 'xs', color: 'primary' })``;
