import React, { useCallback, useState } from 'react';
import { Modal } from '../../components';
import { Container, SubmitButton, Title, Input } from './add-resource.style';

interface AddResourceProps {
  isOpen: boolean;
  onClose: () => void;
  onSubmit: (arg0: any) => void;
}

const AddResource = ({ isOpen, onClose, onSubmit }: AddResourceProps) => {
  const [title, setTitle] = useState('');
  const [body, setBody] = useState('');

  return (
    <Modal isOpen={isOpen} onClose={onClose}>
      <Container>
        <Title>Create a new resource</Title>
        <Input placeholder="Enter the title" value={title} onChange={(e) => setTitle(e.target.value)} />
        <Input placeholder="Enter the body" value={body} onChange={(e) => setBody(e.target.value)} />
        <SubmitButton onClick={() => onSubmit({ title, body })}>Submit</SubmitButton>
      </Container>
    </Modal>
  );
};

export default AddResource;
