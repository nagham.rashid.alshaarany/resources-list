import styled from 'styled-components';
import colors, { gray, primary } from '../../Theme/colors';
import { Card as CardBase, Button, Typography, Badge as BaseBadge } from '../../components';
import { device, size } from '../../Theme/breakpoints';
import { shadow } from '../../Theme';

export const Container = styled.div``;

export const Header = styled.div`
  background: ${primary[50]};
  clip-path: polygon(0% 0%, 100% 0%, 100% calc(100% - 50px), 0% 100%);

  padding-bottom: 56px;
  width: 100%;
`;

export const ShapeContainer = styled.div`
  position: relative;
  z-index: -1;
`;

const Shape = styled.div`
  position: absolute;
  clip-path: polygon(0% 50px, 100% 0%, 100% calc(100% - 50px), 0% 100%);
  height: 128px;

  left: 0;
  right: 0;
  top: -50;
`;

export const Shape0 = styled(Shape)`
  background: ${primary[50]};
  clip-path: polygon(0% 100px, 100% 0%, 100% calc(100% - 1-0px), 0% 100%);
  height: 256px;
  top: -50px;
  z-index: 0;
`;
export const Shape1 = styled(Shape)`
  background: linear-gradient(120deg, ${primary[50]} 0%, ${primary[300]} 100%);
  top: calc(2 * (128px - 50px) + 156px);
`;
export const Shape2 = styled(Shape)`
  background: linear-gradient(120deg, ${primary[100]} 0%, ${primary[200]} 100%);
  top: calc(1 * (128px - 50px) + 156px);
`;
export const Shape3 = styled(Shape)`
  background: linear-gradient(120deg, ${primary[300]} 0%, ${primary[100]} 100%);
  top: 156px;
`;

export const Content = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  padding-right: 16px;
  padding-left: 16px;

  margin-right: auto;
  margin-left: auto;

  max-width: ${size.laptopL};

  @media ${device.mobileS} {
    padding-top: 48px;
  }

  @media ${device.tablet} {
    padding-top: 100px;
  }
`;

export const Title = styled(Typography).attrs({ variant: 'semibold', size: 'lg', color: primary[900] })`
  margin-top: 16px;

  @media ${device.mobileS} {
    text-align: left;
  }
  @media ${device.tablet} {
    text-align: center;
  }
`;

export const SubTitle = styled(Typography).attrs({ variant: 'normal', size: 'xl', color: primary[700] })`
  margin-top: 24px;
`;

export const AddButton = styled(Button).attrs({ variant: 'contained', size: 'xs', color: 'primary' })`
  margin-top: 40px;

  @media ${device.mobileS} {
    width: 100%;
  }

  @media ${device.mobileL} {
    width: unset;
  }

  @media ${device.tablet} {
    margin-right: unset;
  }
`;

export const CardsContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;

  padding-right: 16px;
  padding-left: 16px;

  margin-right: auto;
  margin-left: auto;

  max-width: ${size.laptopL};
  margin-bottom: 128px;
`;

export const CardsRow = styled.div`
  display: flex;
  flex-wrap: wrap;

  margin-left: -16px;
  margin-right: -16px;
`;

export const CardCol = styled.div`
  flex-basis: 0;
  flex-grow: 1;

  @media ${device.mobileS} {
    flex: 0 0 100%;
    max-width: 100%;
  }

  @media ${device.tablet} {
    flex: 0 0 50%;
    max-width: 50%;
  }

  @media ${device.laptop} {
    flex: 0 0 33.3333%;
    max-width: 33.3333%;
  }
`;

export const Card = styled(CardBase)`
  @media ${device.mobileS} {
    margin: 16px;
  }

  @media ${device.tablet} {
    margin: 24px 16px;
  }
`;

export const HorizontalLine = styled.div`
  width: 100%;
  border-style: solid;
  border-color: ${gray[200]};
  border-top: 1px;
`;

export const BottomStick = styled.div`
  position: fixed;
  padding-left: 16px;
  padding-right: 16px;
  left: 0;
  right: 0;
  bottom: 0;

  background-color: ${colors.white};
  box-shadow: ${shadow.xs};
  border-top: 1px solid ${colors.gray[100]};
`;

export const Badge = styled(BaseBadge)`
  @media ${device.mobileS} {
    display: none;
  }
  @media ${device.tablet} {
    display: unset;
  }
`;
