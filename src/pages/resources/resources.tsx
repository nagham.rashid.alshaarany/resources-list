import React, { useCallback, useEffect, useState } from 'react';
import {
  Container,
  Header,
  ShapeContainer,
  Shape0,
  Shape1,
  Shape2,
  Shape3,
  Content,
  Title,
  SubTitle,
  AddButton,
  CardsContainer,
  Card,
  CardCol,
  CardsRow,
  BottomStick
} from './resources.style';
import Badge from '../../components/Badge/Badge';
import { RootState, useAppDispatch } from '../../redux/store';
import { useSelector } from 'react-redux';
import { getPosts } from '../../redux/reducers/postSlice';
import Pagination from '../../components/Pagination';
import ErrorMessage from '../error';
import AddResource from '../add-resource/add-resource';
import EditResource from '../edit-resource/edit-resource';
import { title } from 'process';
import { createPost } from '../../redux/reducers/createPostSlice';

interface Post {
  userId: number;
  id: number;
  title: string;
  body: string;
}

const Resources = () => {
  const page = useSelector((state: RootState) => state.pagination.page);
  const perPage = useSelector((state: RootState) => state.pagination.perPage);

  const dispatch = useAppDispatch();
  useEffect(() => {
    const start = (page - 1) * perPage;
    dispatch(getPosts({ start: start, limit: perPage }));
  }, [dispatch, page, perPage]);

  const { data: posts, total, loading, error } = useSelector((state: RootState) => state.posts);

  const [isAddResourceModalOpen, setAddResourceModalOpen] = useState(false);
  const handleAddResourceModalCloseClick = useCallback(() => {
    setAddResourceModalOpen(false);
  }, []);

  const handleResouceAdd = useCallback((data: any) => {
    dispatch(createPost({ userId: 1, title: data.title, body: data.body }));
    dispatch(getPosts({ start: (page - 1) * perPage, limit: perPage }));
    setAddResourceModalOpen(false);
  }, []);

  return (
    <Container>
      <Header>
        {error ? (
          <ErrorMessage />
        ) : (
          <Content>
            <Badge text="Our blog" />
            <Title> Resources and insights</Title>
            <SubTitle> The latest industry news, interviews, technologies, and resources.</SubTitle>
            <AddButton onClick={() => setAddResourceModalOpen(true)}> Add a new resource </AddButton>
          </Content>
        )}
      </Header>
      <ShapeContainer>
        <Shape0 />
        <Shape1 />
        <Shape2 />
        <Shape3 />
      </ShapeContainer>
      {!loading && !error && (
        <CardsContainer>
          <CardsRow>
            {posts.map((item: Post, key: number) => (
              <CardCol key={key}>
                <Card
                  id={item.id}
                  category={`user-${item.userId}`}
                  imagePath={`https://picsum.photos/320/240?random=${item.id}`}
                  title={item.title}
                  body={item.body}
                  key={key}
                />
              </CardCol>
            ))}
          </CardsRow>
          <BottomStick>
            <Pagination total={total} />
          </BottomStick>{' '}
        </CardsContainer>
      )}

      <AddResource
        onClose={handleAddResourceModalCloseClick}
        isOpen={isAddResourceModalOpen}
        onSubmit={handleResouceAdd}
      />
    </Container>
  );
};

export default Resources;
