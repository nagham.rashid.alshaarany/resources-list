import React from 'react';
import './App.css';
import Resources from './pages/resources';

function App() {
  return (
    <React.Fragment>
      <Resources />
    </React.Fragment>
  );
}

export default App;
