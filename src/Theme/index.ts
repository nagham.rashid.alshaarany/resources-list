import { default as colors } from './colors';
import { device, size } from './breakpoints';
import { shadow } from './shadow';

export { colors, device, size, shadow };
