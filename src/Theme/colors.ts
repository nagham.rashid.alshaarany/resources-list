export const primary = {
  50: '#F9F5FF',
  100: '#F4EBFF',
  200: '#E9D7FE',
  300: '#D6BBFB',
  600: '#7F56D9',
  700: '#6941C6',
  900: '#42307D'
};
export const gray = {
  50: '#F9FAFB',
  100: '#eaecf0',
  200: '#b2b8c1',
  300: '#D0D5DD',
  600: '#475467',
  900: '#101828'
};

export const red = {
  50: '#FDF2FA',
  400: '#F692CE',
  700: '#C11574'
};

export const indig = {
  50: '#EEF4FF',
  400: '#8E96FB',
  700: '#3538CD'
};
export const white = '#ffffff';

const colors = {
  primary,
  gray,
  white,
  red,
  indig
};

export default colors;
