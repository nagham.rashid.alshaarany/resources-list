import {configureStore} from '@reduxjs/toolkit'
import postsReducer from './reducers/postSlice'
import paginationReducer from './reducers/paginationSlice'
import updatePostReducer from './reducers/updatePostSlice'
import deletePostReducer from './reducers/deletePostSlice'
import createPostReducer from './reducers/createPostSlice'
import { TypedUseSelectorHook, useDispatch, useSelector } from 'react-redux'

const store = configureStore({
  reducer: {
    posts: postsReducer,
    pagination: paginationReducer,
    deletePostReducer,
    updatePostReducer,
    createPostReducer
  }
});

export default store
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch = () => useDispatch<AppDispatch>();
export type RootState = ReturnType<typeof store.getState>;
export const useTypedSelector: TypedUseSelectorHook<RootState> = useSelector;
