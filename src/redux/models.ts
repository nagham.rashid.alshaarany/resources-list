export interface PostModel {
  userId: number;
  id: number;
  title: string;
  body: string;
}

export interface PaginationModel {
  start: number;
  limit: number;
}
