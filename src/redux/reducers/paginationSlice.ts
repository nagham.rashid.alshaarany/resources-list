import { createSlice, PayloadAction } from "@reduxjs/toolkit";

interface PaginationState {
  page: number;
  perPage: number;
}

const initialState: PaginationState = {
  page: 1,
  perPage: 12,
};

export const PaginationSlice = createSlice({
  name: "posts",
  initialState,
  reducers: {
    setPagination: (state: PaginationState, action: PayloadAction<PaginationState>) => {
      state.page= action.payload.page;
      state.perPage= action.payload.perPage;
    },
  },
});
export const { setPagination } = PaginationSlice.actions;

export default PaginationSlice.reducer;
