import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

interface PostState {
  loading: boolean;
  error: boolean;
  data: { id: number };
}

const initialState: PostState = {
  loading: false,
  error: false,
  data: { id: 0 }
};

interface PostArg {
  title?: string;
  body?: string;
  postId: number;
}

export const updatePost = createAsyncThunk('posts/update', async (post: PostArg): Promise<{ id: number }> => {
  const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${post.postId}`, {
    method: 'PUT',
    body: JSON.stringify(post)
  });
  return await response.json();
});

export const UpdatePostsSlice = createSlice({
  name: 'update post',
  initialState,
  reducers: {},
  extraReducers: (builder: any) => {
    builder.addCase(updatePost.pending, (state: PostState) => {
      state.loading = true;
    });
    builder.addCase(updatePost.fulfilled, (state: PostState) => {
      state.loading = false;
    });
    builder.addCase(updatePost.rejected, (state: PostState) => {
      state.error = true;
      state.loading = false;
    });
  }
});

export default UpdatePostsSlice.reducer;
