import { createAsyncThunk, createSlice ,PayloadAction} from '@reduxjs/toolkit';
import { PostModel } from '../models';

interface PostsState {
  loading: boolean;
  error: boolean;
  total?: number;
  data: PostModel[];
}

interface PageArg {
  start: number;
  limit: number;
}

const initialState: PostsState = {
  loading: true,
  error: false,
  total: 0,
  data: []
};

export const getPosts = createAsyncThunk(
  'posts/get',
  async ({ start, limit }: PageArg): Promise<{ posts: [PostModel]; total: string | null }> => {
    const response = await fetch(`https://jsonplaceholder.typicode.com/posts?_start=${start}&_limit=${limit}`);
    return { posts: await response.json(), total: response.headers.get('x-total-count') };
  }
);
export const PostsSlice = createSlice({
  name: 'posts',
  initialState,
  reducers: {},
  extraReducers: (builder: any) => {
    builder.addCase(getPosts.pending, (state: PostsState) => {
      state.loading = true;
    });
    builder.addCase(getPosts.fulfilled, (state: PostsState, action: any) => {
      state.data = action.payload.posts;
      state.total = Number(action.payload.total);
      state.loading = false;
    });
    builder.addCase(getPosts.rejected, (state: PostsState) => {
      state.error = true;
      state.loading = false;
    });
  }
});

export default PostsSlice.reducer;
