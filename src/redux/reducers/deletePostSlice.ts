import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

interface PostState {
  loading: boolean;
  error: boolean;
}

const initialState: PostState = {
  loading: false,
  error: false
};

export const deletePost = createAsyncThunk('posts/delete', async (postId: number): Promise<{}> => {
  const response = await fetch(`https://jsonplaceholder.typicode.com/posts/${postId}`, {
    method: 'DELETE'
  });
  return await response.json();
});

export const DeletePostsSlice = createSlice({
  name: 'delete post',
  initialState,
  reducers: {},
  extraReducers: (builder: any) => {
    builder.addCase(deletePost.pending, (state: PostState) => {
      state.loading = true;
    });
    builder.addCase(deletePost.fulfilled, (state: PostState) => {
      state.loading = false;
    });
    builder.addCase(deletePost.rejected, (state: PostState) => {
      state.error = true;
      state.loading = false;
    });
  }
});

export default DeletePostsSlice.reducer;
