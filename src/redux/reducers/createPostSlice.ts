import { createAsyncThunk, createSlice, PayloadAction } from '@reduxjs/toolkit';

interface PostState {
  loading: boolean;
  error: boolean;
  data: { id: number };
}

const initialState: PostState = {
  loading: false,
  error: false,
  data: { id: -1 }
};

interface PostArg {
  userId: number;
  title: string;
  body: string;
}

export const createPost = createAsyncThunk('posts/create', async (newPost: PostArg): Promise<{ id: number }> => {
  const response = await fetch(`https://jsonplaceholder.typicode.com/posts`, {
    method: 'POST',
    body: JSON.stringify(newPost)
  });
  return await response.json();
});

export const CreatePostsSlice = createSlice({
  name: 'create post',
  initialState,
  reducers: {},
  extraReducers: (builder: any) => {
    builder.addCase(createPost.pending, (state: PostState) => {
      state.loading = true;
    });
    builder.addCase(createPost.fulfilled, (state: PostState, action: PayloadAction<PostState>) => {
      state.data = action.payload.data;
      state.loading = false;
    });
    builder.addCase(createPost.rejected, (state: PostState) => {
      state.error = true;
      state.loading = false;
    });
  }
});

export default CreatePostsSlice.reducer;
