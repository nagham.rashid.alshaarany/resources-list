import styled from "styled-components";

export const variantsMapping: { [char: string]: any} = {
    h1: styled.h1,
    h2: styled.h2,
    h3: styled.h3,
    h4: styled.h4,
    h5: styled.h5,
    h6: styled.h6,
  
    subheading1: styled.h6,
    subheading2: styled.h6,
    body1: styled.p,
    body2: styled.p,
    
    medium: styled.span,
    semibold: styled.span,
    normal: styled.span
  } as const;


export const Element = styled.span`
    /* Color */
    color: ${props => props.color};

    /* Font */
    &.varaint-medium-size-sm{
        font-family: Inter;
        font-style: normal;
        font-size: 14px;
        font-weight: 500;
        line-height: 20px;
    }

    /* Display lg/Semibold */
    &.varaint-semibold-size-lg{
        font-family: 'Inter';
        font-style: normal;
        font-weight: 600;
        font-size: 48px;
        line-height: 60px;
    }

    /* Display sm/Semibold */
    &.varaint-semibold-size-xs{
        font-family: 'Inter';
        font-style: normal;
        font-weight: 600;
        font-size: 24px;
        line-height: 32px;
    }
    
    /* Display sm/Semibold */
    &.varaint-semibold-size-sm{
        font-family: 'Inter';
        font-style: normal;
        font-weight: 600;
        font-size: 14px;
        line-height: 20px;
    }

    /* Text xl/Normal */
    &.varaint-normal-size-xl{
        font-family: 'Inter';
        font-style: normal;
        font-weight: 400;
        font-size: 20px;
        line-height: 30px;
    }

    /* Text md/Normal */
    &.varaint-normal-size-md{
        font-family: 'Inter';
        font-style: normal;
        font-weight: 400;
        font-size: 16px;
        line-height: 24px;
    }



`
