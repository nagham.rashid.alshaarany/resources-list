import React from "react";
import {Element} from './Typography.style'
import cn from "classnames"

interface TypographyProps {
    variant: 'h1' | 'h2' | 'h3' | 'h4' | 'h4' | 'h6' | 'subheading1' | 'subheading2' | 'body1' | 'body2' | 'medium' | 'semibold' | 'normal',
    size: 'xs' | 'sm' | 'md' | 'lg' | 'xl',
    color: string,
    className?: string,
    children?: any
}

const Typography = ({className, ...props}: TypographyProps) => {
    const classes = cn(className, `varaint-${props.variant}-size-${props.size}`)
    
    return (
        <Element className={classes} {...props}/>
    );
}

export default Typography;