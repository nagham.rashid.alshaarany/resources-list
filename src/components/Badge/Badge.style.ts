import styled from "styled-components";
import { primary } from "../../Theme/colors";

export const Container = styled.span`
    padding: 4px 12px;
    border-radius: 16px;
    background-color: ${primary[100]};
    vertical-align: middle;
`