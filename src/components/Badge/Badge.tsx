import React from 'react';
import { primary } from '../../Theme/colors';
import Typography from '../Typography';
import { Container } from './Badge.style';

interface BadgeProps {
  onClick?: any;
  text: string;
  className?: string;
}
const Badge = ({ text, onClick, className }: BadgeProps) => {
  return (
    <Container onClick={onClick} className={className}>
      <Typography variant="medium" size="sm" color={primary[700]}>
        {text}
      </Typography>
    </Container>
  );
};

export default Badge;
