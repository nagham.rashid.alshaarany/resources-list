import styled from 'styled-components';
import colors from '../../Theme/colors';
import { shadow } from '../../Theme/shadow';
import Typography from '../Typography';
import Button from '../Button';

export const Container = styled.div`
  background-color: ${colors.white};
  box-shadow: ${shadow.lg};
  padding: 24px;
  border-radius: 4px;

  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Image = styled.img`
  width: 100%;
  height: auto;
`;

export const Category = styled(Typography).attrs({ variant: 'semibold', size: 'sm', color: colors.primary[700] })`
  margin-top: 32px;
`;

export const Title = styled(Typography).attrs({ variant: 'semibold', size: 'xs', color: colors.gray[900] })`
  margin-top: 12px;
  word-wrap: break-word;
`;

export const Body = styled(Typography).attrs({ variant: 'normal', size: 'md', color: colors.gray[600] })`
  margin-top: 8px;
`;

export const ActionsContainer = styled.div`
  width: 100%;
  margin-top: 24px;

  > :not(:last-child) {
    margin-right: 8px;
  }
`;

export const Action = styled(Button).attrs({ variant: 'badge', size: 'xs', color: 'success' })`
  border-radius: 16px;
  padding: 4px 12px;

  &.edit {
    background-color: ${colors.indig[50]};
    color: ${colors.indig[700]};
    border: 1px solid ${colors.indig[700]};
    &:hover {
      background-color: ${colors.indig[400]};
    }
  }
  &.delete {
    background-color: ${colors.red[50]};
    color: ${colors.red[700]};
    border: 1px solid ${colors.red[700]};
    &:hover {
      background-color: ${colors.red[400]};
    }
  }
`;
