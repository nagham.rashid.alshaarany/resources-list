import React, { useCallback, useState } from 'react';
import { useSelector } from 'react-redux';
import { deletePost } from '../../redux/reducers/deletePostSlice';
import { getPosts } from '../../redux/reducers/postSlice';
import { updatePost } from '../../redux/reducers/updatePostSlice';
import { RootState, useAppDispatch } from '../../redux/store';
import { Container, Image, Category, Title, Body, ActionsContainer, Action } from './Card.style';
import EditResource from '../../pages/edit-resource';
import DeleteResource from '../../pages/delete-resource';

interface CardProps {
  category: string;
  title: string;
  body: string;
  className?: string;
  id: number;
  imagePath: string;
}

const Card = ({ id, title, body, category, imagePath, className }: CardProps) => {
  const dispatch = useAppDispatch();

  const page = useSelector((state: RootState) => state.pagination.page);
  const perPage = useSelector((state: RootState) => state.pagination.perPage);

  const reFetch = useCallback(() => {
    dispatch(getPosts({ start: (page - 1) * perPage, limit: perPage }));
  }, []);

  const handleDeleteClick = useCallback(() => {
    dispatch(deletePost(id));
    reFetch();
  }, []);

  const handleUpdateClick = useCallback((data: any) => {
    dispatch(updatePost({ postId: id, title: data.title, body: data.body }));
    reFetch();
  }, []);

  const [isEditModalOpen, setIsEditModalOpen] = useState(false);
  const handleEditClose = useCallback(() => {
    setIsEditModalOpen(false);
  }, []);

  const [isDeleteModalOpen, setIsDeleteModalOpen] = useState(false);
  const handleDeleteClose = useCallback(() => {
    setIsDeleteModalOpen(false);
  }, []);

  return (
    <Container className={className}>
      <Image src={imagePath} />
      <Category>{category}</Category>
      <Title>{title}</Title>
      <Body>{body}</Body>
      <ActionsContainer>
        <Action className="edit" onClick={() => setIsEditModalOpen(true)}>
          Edit
        </Action>
        <Action className="delete" onClick={() => setIsDeleteModalOpen(true)}>
          Delete
        </Action>
      </ActionsContainer>
      <EditResource
        data={{ title, body }}
        onSubmit={handleUpdateClick}
        onClose={handleEditClose}
        isOpen={isEditModalOpen}
      />

      <DeleteResource
        data={{ title }}
        onSubmit={handleDeleteClick}
        onClose={handleDeleteClose}
        isOpen={isDeleteModalOpen}
      />
    </Container>
  );
};

export default Card;
