import React, { Fragment } from 'react';
import { useSelector } from 'react-redux';
import cn from 'classnames';
import { setPagination } from '../../redux/reducers/paginationSlice';
import { RootState, useAppDispatch } from '../../redux/store';
import {
  Container,
  FLButton,
  FLButtonSmall,
  PaginatorButton,
  PaginatorPages,
  PreviousIcon,
  NextIcon,
  Gap
} from './pagination.style';

interface PaginationProps {
  total: number | undefined;
  className?: string;
}

const Pagination = ({ total }: PaginationProps) => {
  if (total === undefined) total = 0;
  const dispatch = useAppDispatch();
  const handleClick = (page: number) => {
    dispatch(setPagination({ page: page, perPage: 12 }));
  };

  const page = useSelector((state: RootState) => state.pagination.page);
  const perPage = useSelector((state: RootState) => state.pagination.perPage);
  const cntPage = Math.ceil(total / perPage);

  const pageOptions: number[] = [];
  pageOptions.push(1);
  for (let i = Math.max(2, page - 1); i <= Math.min(cntPage - 1, page + 1); i++) pageOptions.push(i);
  pageOptions.push(cntPage);

  return (
    <Container>
      <FLButton className="first" disabled={page == 1} onClick={() => handleClick(page - 1)}>
        <PreviousIcon /> <span>Previous</span>
      </FLButton>
      <FLButtonSmall disabled={page == 1} onClick={() => handleClick(page - 1)}>
        <PreviousIcon />
      </FLButtonSmall>

      <PaginatorPages>
        {pageOptions.map((number, index) => {
          const gap = index > 0 && pageOptions[index - 1] + 1 != number;

          return (
            <Fragment key={index}>
              {gap && <Gap />}
              <PaginatorButton className={cn({ active: page == number })} onClick={() => handleClick(number)}>
                {number}
              </PaginatorButton>
            </Fragment>
          );
        })}
      </PaginatorPages>

      <FLButton className="last" disabled={page == cntPage} onClick={() => handleClick(page + 1)}>
        <span>Next</span> <NextIcon />
      </FLButton>

      <FLButtonSmall disabled={page == cntPage} onClick={() => handleClick(page + 1)}>
        <NextIcon />
      </FLButtonSmall>
    </Container>
  );
};

export default Pagination;
