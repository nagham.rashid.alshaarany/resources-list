import styled from 'styled-components';
import Button from '../Button';
import PreviousIconSVG from '../../assets/Icons/left-arrow.svg';
import NextIconSVG from '../../assets/Icons/right-arrow.svg';
import { device, colors, shadow } from '../../Theme';

export const Container = styled.div`
  display: flex;
  flex-grow: 1;
  justify-content: space-between;
  align-items: center;
  align-content: center;
  padding-top: 20px;
  padding-bottom: 30px;
`;

export const PaginatorPages = styled.div`
  margin-left: auto;
  margin-right: auto;
  padding: 0px;
  gap: 4px;
`;

export const PaginatorButton = styled(Button).attrs({ variant: 'none', size: 'xs', color: 'none' })`
  width: 40px;
  height: 40px;
  border-radius: 8px;

  &.active {
    background-color: ${colors.gray[50]};
  }
`;

export const FLButton = styled(Button).attrs({ variant: 'none', size: 'xs', color: 'none' })`
  @media ${device.mobileS} {
    display: none;
  }
  @media ${device.tablet} {
    display: block;
  }
  &.first > img {
    margin-right: 12px;
  }

  &.last > img {
    margin-left: 12px;
  }
`;

export const FLButtonSmall = styled.button`
  width: 36px;
  height: 36px;
  background: ${colors.white};
  border: 1px solid ${colors.gray[300]};
  box-shadow: ${shadow.xs};
  border-radius: 8px;

  display: flex;
  justify-content: center;
  align-items: center;

  @media ${device.mobileS} {
    display: block;
  }
  @media ${device.tablet} {
    display: none;
  }
`;

export const PreviousIcon = styled.img.attrs({
  src: PreviousIconSVG
})`
  width: 12px;
  height: 12px;
`;

export const NextIcon = styled.img.attrs({
  src: NextIconSVG
})`
  width: 12px;
  height: 12px;
`;

export const Gap = styled.span`
  height: 40px;
  line-height: 30px;
  &::before {
    content: '...';
  }
`;
