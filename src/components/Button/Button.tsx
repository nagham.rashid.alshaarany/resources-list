import React from 'react';
import cn from 'classnames';
import { Element } from './Button.style';

interface ButtonProps {
  variant: 'contained' | 'outlined' | 'text' | 'badge' | 'none';
  size: 'xs' | 'sm' | 'md' | 'lg' | 'xl';
  color?: 'primary' | 'success' | 'danger' | 'none' | 'gray';
  className?: string;
  children?: any;
  onClick?: any;
  disabled?: boolean;
}

const Button = ({ className, onClick, ...props }: ButtonProps) => {
  const classes = cn(className, `varaint-${props.variant}-size-${props.size}`, `color-${props.color}`);

  return <Element className={classes} onClick={onClick} {...props} />;
};

export default Button;
