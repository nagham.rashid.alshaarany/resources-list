import styled from 'styled-components';
import colors from '../../Theme/colors';
import { shadow } from '../../Theme/shadow';

export const Element = styled.button`
  padding: 12px 20px;
  border-radius: 8px;
  border-width: 1px;
  border-style: solid;

  box-shadow: ${shadow['xs']};
  transition: background-color 100ms ease-out;

  &:hover:not(:disabled) {
    cursor: pointer;
  }

  &.color-primary {
    border-color: ${colors.primary[600]};
    background: ${colors.primary[600]};
    color: ${colors.white};
  }

  &.color-success {
    border-color: ${colors.primary[100]};
    background: ${colors.primary[100]};
    color: ${colors.primary[600]};
  }

  &.color-gray {
    border-color: ${colors.gray[200]};
    background: ${colors.primary[50]};
    color: ${colors.gray[900]};
  }

  &.color-none {
    border-color: unset;
    background: unset;
    border-width: 0;
    box-shadow: unset;
    color: ${colors.gray[900]};
  }

  &.color-danger {
    background-color: ${colors.red[50]};
    color: ${colors.red[700]};
    border: 1px solid ${colors.red[700]};
  }

  &.varaint-none-size-xs {
    padding: 0;
  }
`;
