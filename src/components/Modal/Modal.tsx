import React, { ReactNode } from 'react';
import { ModalOverlay, ModalWrapper } from './Modal.style';

type ModalProps = {
  isOpen: boolean;
  onClose: () => void;
  children: ReactNode;
  varaint?: 'primary' | 'danger';
};

const Modal = ({ isOpen, onClose, children, varaint = 'primary' }: ModalProps) => {
  if (!isOpen) return null;

  return (
    <ModalOverlay onClick={onClose}>
      <ModalWrapper className={varaint} onClick={(e: any) => e.stopPropagation()}>
        {children}
      </ModalWrapper>
    </ModalOverlay>
  );
};

export default Modal;
