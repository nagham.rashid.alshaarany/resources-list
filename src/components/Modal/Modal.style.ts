import styled from 'styled-components';
import { colors, device } from '../../Theme';

export const ModalOverlay = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  background-color: rgba(0, 0, 0, 0.5);
  z-index: 999;
`;

export const ModalWrapper = styled.div`
  position: fixed;
  top: 50%;
  z-index: 1000;
  padding: 32px;

  background-color: ${colors.white};
  background: ${colors.gray[50]};
  border-radius: 0px 0px 16px 16px;

  &.primary {
    border-top: 4px solid ${colors.primary[700]};
  }
  &.danger {
    border-top: 4px solid ${colors.red[700]};
  }

  @media ${device.mobileS} {
    left: 32px;
    right: 32px;
    transform: translate(0, -50%);
  }

  @media ${device.tablet} {
    left: 50%;
    transform: translate(-50%, -50%);
  }
`;
